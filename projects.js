'use strict'

// PROJECT MENU
let proj_menu_obj = []
let proj_menu_data = []
// MAIN TEXT
let main_text = document.getElementById('main_text')
let main_text_data = {}
proj_menu_obj.push(main_text)
proj_menu_data.push(main_text_data)
// PROJECT DESCRIPTION
let proj_desc = document.getElementById('proj_desc')
let proj_desc_data = {opacity: {a: 0, b: 1}}
proj_menu_obj.push(proj_desc)
proj_menu_data.push(proj_desc_data)
// SIDEBAR LEFT
let sidebar_L = document.getElementById('sidebar_L')
let sidebar_L_data = {}
proj_menu_obj.push(sidebar_L)
proj_menu_data.push(sidebar_L_data)
// SIDEBAR RIGHT
let sidebar_R = document.getElementById('sidebar_R')
let sidebar_R_data = {opacity: {a: 0, b: 0.5}}
proj_menu_obj.push(sidebar_R)
proj_menu_data.push(sidebar_R_data)
// PROJECT NAME
let proj_name = document.getElementById('proj_name')
let proj_name_data = {opacity: {a: 0, b: 1}}
proj_menu_obj.push(proj_name)
proj_menu_data.push(proj_name_data)
// PREVIEW IMAGES
let proj_imagess = []
let proj_image_data = {opacity: {a: 1, b: 0}}
document.getElementsByName('proj_images').forEach(image => {
    proj_menu_obj.push(image)
    proj_imagess.push(image)
})

// PROJECT LIST
let proj_list = document.getElementById('proj_list')

let proj_list_entries = []
// update project list heights
for (let i = 0, row; row = proj_list.rows[i]; i++) {
    for (let j = 0, cell; cell = row.cells[j]; j++) {
        cell.style.top = 25 + (i * 15) + "%"
        proj_menu_obj.push(cell)
        proj_menu_data.push({opacity: {a: 1, b: 0}})
        proj_list_entries.push(cell)
    }
}

let submenu_visibility = false

// preload images
let proj_images = [...document.getElementsByName('proj_image')]


// add event listener for title text
main_text.addEventListener('click', function() {
    if (submenu_visibility) {
        scroll_top(true)
        // toggle clickable title text
        main_text.style.cursor = (!submenu_visibility) ? 'pointer' : 'default'
        // toggles clickable & visibility for respective items
        toggle_clickable(proj_list_entries, submenu_visibility)
        toggle_submenu_visibility()
    }
})
// add event listeners for project list
for (let i = 0; i < proj_list_entries.length; i++) {
    proj_list_entries[i].addEventListener('click', function() {
        if (!submenu_visibility) {
            scroll_top()
            // toggle clickable title text
            main_text.style.cursor = (!submenu_visibility) ? 'pointer' : 'default'
            // toggles clickable & visibility for respective items
            toggle_clickable(proj_list_entries, submenu_visibility)
            update_desc(proj_list_entries[i].id)
            toggle_submenu_visibility(proj_list_entries[i].id)
        }
    })
}

function toggle_clickable(toggle_list, clickable) {
    for (let i = 0; i < toggle_list.length; i++) {
        toggle_list[i].style.cursor = (clickable) ? 'pointer' : 'default'
    }
}

function toggle_submenu_visibility(id=-1) {
    submenu_visibility = !submenu_visibility
    document.body.style.overflow = (submenu_visibility) ? '' : 'hidden'
    // update preview image
    proj_images.forEach(image => {
        image.style.opacity = (submenu_visibility && id == image.id) ? proj_image_data.opacity.a : proj_image_data.opacity.b
        image.style.transition = (submenu_visibility) ? 'opacity 0.5s ease-in 0.3s' : 'opacity 0.3s ease-in 0s'
    })
    // update transition times
    // -- change to use data later
    proj_name.style.transition = (submenu_visibility) ? 'opacity 0.5s ease-in 0.3s' : 'opacity 0.3s ease-in 0s'
    proj_desc.style.transition = (submenu_visibility) ? 'opacity 0.5s ease-in 0.2s' : 'opacity 0.3s ease-in 0s'
    for (let i = 0; i < proj_list_entries.length; i++) {
        proj_list_entries[i].style.transition = (submenu_visibility) ? 'opacity 0.3s ease-in 0s' : 'opacity 0.5s ease-in 0.2s'
    }
    // change opacity
    for (let i = 0; i < proj_menu_data.length; i++) {
        if (proj_menu_data[i].opacity) {
            proj_menu_obj[i].style.opacity = submenu_visibility
            ? proj_menu_data[i].opacity.b : proj_menu_data[i].opacity.a
        }
    }
}

function scroll_top(reset_text) {
    var currentPosition = window.scrollY;
    if (currentPosition > 0) {
        window.scrollTo(0, currentPosition - 10)
        requestAnimationFrame(scroll_top);
    }
    if (reset_text) {
        document.body.style.overflow = 'hidden'
        setTimeout(() => {
            console.log(proj_desc.style.opacity)
            if (proj_desc.opacity == 0) {
                proj_desc.innerText = ''
            }
        }, 500)
    }
}


function update_desc(id) {
    let names = [
        'Project LRD',
        'Python TCG',
        'Fire Emblem JS',
        'shortHTML'
    ]
    let descriptions = [
    `\
    Project LRD is a retro-style RPG with a focus on player interaction that provides solo or team-based gameplay, extensive PvE with various other types of combat, \
    epic bosses, secret areas and hidden treasures, all contained in a persistent, semi-randomized online world with real-time quests. \nThe inspiration for this game \
    originates \nwith classic titles such as Legend of the \nRed Dragon, Final Fantasy, even Oregon Trail, and many more. 

    The purpose: to focus on interworking mechanics, strategic depth, and above all, fun gameplay, in order to create a unique and enjoyable experience.\n\n

    `,
    `
    Python Tabletop Card Game (or PyTCG) is a card game simulator designed to support nearly any card game for online play. The most interesting aspect of this project \
    is the method used to load card images. Image URLs are saved instead of the images themselves, allowing images to be pulled from an online database to display in-game. \
    This means practically any card from any game or even custom user-made cards are supported natively and it saves significant space on the user's device as a bonus. \
    This method incurs minimal load times, being capable of loading an entire deck of 60 cards in just over 5 seconds on average \nwith a stable internet connection. 

    PyTCG is nearing release for v1.0 and will continue to be updated to provide support for more card games over time.\n\n

    `,
    `
    This project, commonly abbreviated as FEjs, is a recreation of the Fire Emblem Heroes gameplay engine, written in JavaScript. The goal of this project was to develop \
    the game engine and learn to create games in the JS language. In all, it has a functional tile-grid map with movement, animations, in-combat and overworld skills, and combat \
    calculations. The AI for enemy characters is partially programmed, but not completed. There are plans to build a second version of the FEjs engine in order to create a fully \
    original game which will begin development at a later date.

    `,
    `
    This is one of my early JavaScript projects, designed to make HTML code very compact and efficient to write, at the cost of legibility. Essentially it just finds tags \
    in the user-submitted code and performs actions based on those tags. There is some neat functionality provided, such as support for variables, functions, groups, math and \
    other types of operations, ASM-esque goto and read, automatic tag management, and a few more. Most likely there will be a heavily reworked and refined v2.0 in the future. 

    `
    ]
    proj_name.innerText = names[id]
    proj_desc.innerText = descriptions[id]
}
